import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

let maxId = 0;

const state = {
  toastList: [],
};

const getters = {
  getToastIndexById: (state) => (id) => {
    return state.toastList.findIndex((toast) => toast.id === id);
  },
};

const actions = {
  addToast({ commit }, message) {
    const newToast = {
      id: ++maxId,
      message,
      timer: null,
    };

    commit("addToast", newToast);
  },
  removeToast({ getters, commit }, id) {
    const idx = getters.getToastIndexById(id);

    if (idx !== -1) {
      commit("removeToast", idx);
    }
  },
  setTimer({ getters, dispatch, commit }, { id, duration }) {
    const idx = getters.getToastIndexById(id);

    if (idx !== -1) {
      const timer = setTimeout(() => {
        dispatch("removeToast", id);
      }, duration);

      commit("setTimer", { idx, timer });
    }
  },
  removeTimer({ getters, commit }, id) {
    const idx = getters.getToastIndexById(id);

    if (idx !== -1) {
      commit("removeTimer", idx);
    }
  },
};

const mutations = {
  addToast(state, toast) {
    state.toastList.push(toast);
  },
  removeToast(state, idx) {
    state.toastList.splice(idx, 1);
  },
  setTimer(state, { idx, timer }) {
    state.toastList[idx].timer = timer;
  },
  removeTimer(state, idx) {
    clearTimeout(state.toastList[idx].timer);
  },
};

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  strict: process.env.NODE_ENV !== "production",
});
